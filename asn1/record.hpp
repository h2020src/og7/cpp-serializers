//
// Created by xma on 20/03/19.
//

#ifndef CPP_SERIALIZERS_RECORD_HPP
#define CPP_SERIALIZERS_RECORD_HPP

#include <asn1/Types/C/record.h>

namespace asn1_test {

    BitStream initBitStream(long size)
    {
        BitStream bitStream;
        byte* buffer = new byte[size];
        BitStream_Init(&bitStream, buffer, size);
        return bitStream;
    }

    void destroyBitStream(BitStream& bitStream)
    {
        delete bitStream.buf;
    }



} // namespace

#endif //CPP_SERIALIZERS_RECORD_HPP
